from django.contrib import admin
from .models import Car, CarReserv, Driver, Rate

admin.site.register(Car)
admin.site.register(CarReserv)
admin.site.register(Driver)
admin.site.register(Rate)


# Register your models here.
