from django.db import models
from django.db import models
from django.shortcuts import reverse
from django.utils import timezone
from django.contrib.auth.models import User
from random import randint

RATE = (
    ('1','1'),
    ('2','2'),
    ('3','3'),
    ('4','4'),
    ('5','5'),
    ('6','6'),
    ('7','7'),
    ('8','8'),
    ('9','9'),
    ('10','10'),
)

class Rate(models.Model):
    rate1 = models.IntegerField(choices=RATE)
    date_created = models.DateTimeField(default=timezone.now,editable=False)

    def __str__(self):
        return str(self.rate)

class Car(models.Model):
    car_model = models.CharField(max_length = 30)
    car_color = models.CharField (max_length = 20)
    has_ac = models.BooleanField()
    date_created = models.DateTimeField(default=timezone.now,editable=False)
    booked = models.DateTimeField(default=timezone.now,editable=False)
    
    def __str__(self):
        return str(self.car_model)

class CarReserv(models.Model):
    client = models.ForeignKey(User, on_delete = models.CASCADE)
    from_destination = models.CharField(max_length = 100)
    to_destination = models.CharField(max_length =100)
    price = models.SmallIntegerField()
    duration = models.CharField(max_length = 50, default=randint(10, 30))
    passangers = models.SmallIntegerField()
    car = models.ForeignKey(Car, on_delete=models.CASCADE) 
    date_created = models.DateTimeField(default=timezone.now,editable=False)

    def __str__(self):
        return str(self.date_created)

class Driver(models.Model):
    user = models.OneToOneField(CarReserv, on_delete=models.CASCADE)
    experience = models.SmallIntegerField()
    phone_number = models.CharField(max_length=20)
    rates = models.ManyToManyField(Rate)
    date_created = models.DateTimeField(default=timezone.now,editable=False)

    def __str__(self):
        return str(self.user)

class UserCreate(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=50)
    username = models.CharField(max_length=50)
    password = models.SmallIntegerField()
    conf_password = models.SmallIntegerField()
