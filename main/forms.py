from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms.models import ModelForm
from .models import CarReserv
from django import forms
from django.conf import settings
from django.core.mail import send_mail

class CarReservCreationForm(ModelForm):
    class Meta:
        model = CarReserv
        fields = ('from_destination','to_destination','passangers',)

class CustomUserCreationForm(UserCreationForm):
    first_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
            'class': 'form-control',
            'placeholder': 'First Name'
        }
        )
    )

    last_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
            'class': 'form-control',
            'placeholder': 'Last Name'
        }
        )
    )

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
            'class': 'form-control',
            'placeholder': 'Email'
        }
        )
    )

    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
            'class': 'form-control',
            'placeholder': 'Username'
        }
        )
    )

    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
            'class': 'form-control',
            'placeholder': 'Password'
        }
        )
    )

    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
            'class': 'form-control',
            'placeholder': 'Confirm Password'
        }
        )
    )

    class Meta:
        model = User
        fields = ('first_name','last_name','email','username','password1','password2',)

###############################################
###############################################

class ContactForm(forms.Form):

    name = forms.CharField(max_length=120)
    email = forms.EmailField()
    inquiry = forms.CharField(max_length=70)
    message = forms.CharField(widget=forms.Textarea)

    def get_info(self):
        cl_data = super().clean()

        name = cl_data.get('name').strip()
        from_email = cl_data.get('email')
        subject = cl_data.get('inquiry')

        msg = f'{name} with email {from_email} said:'
        msg += f'\n"{subject}"\n\n'
        msg += cl_data.get('message')

        return subject, msg

    def send(self):

        subject, msg = self.get_info()

        send_mail(
            subject=subject,
            message=msg,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[settings.RECIPIENT_ADDRESS]
        )