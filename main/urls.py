from main.models import CarReserv
from . import views
from django.urls import path
from .views import UloaxView,  CarReservView, AboutView, RegistationView, BookHistoryView
from django.contrib.auth.views import LoginView, LogoutView

app_name = 'main'

urlpatterns = [
    path('', UloaxView.as_view(), name='main'),
    path('about/', AboutView.as_view(), name='about'),
    path('booking/', CarReservView.as_view(), name='booking'),
    path('registration/', RegistationView.as_view(), name = 'registration'),
    path('login/', LoginView.as_view(template_name='main/login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='main/logout.html'), name='logout'),
    path('history/', BookHistoryView.as_view(), name='history')
]
