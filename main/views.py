from django.shortcuts import render
from django.views.generic import ListView, View
from .models import Car, CarReserv, Driver, Rate
from .forms import CarReservCreationForm, CustomUserCreationForm
from django.contrib.auth.models import User
from random import randint
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin

from django.contrib import messages 
from datetime import datetime, timedelta
from django.core.mail import send_mail
from django.conf import settings

class UloaxView(View):
    def get(self, request):
        return render(request=request, template_name='main/taxi_plans.html')

    def post(self, request):
        return render(request=request, template_name='main/book_history.html')

class AboutView(View):
    def get(self, request):
        return render(request=request, template_name='main/about.html')
    
    def post(self, request):
        if "searchreserv" in request.POST:
            startdate_form = request.POST.get('startdate')
            enddate_form = request.POST.get('enddate')

            startdate = datetime.strptime(startdate_form, '%Y-%m-%d')
            enddate = datetime.strptime(enddate_form, '%Y-%m-%d')

            history_reserv = CarReserv.objects.filter(date_created__range=[startdate, enddate])

            return render(request=request, template_name='main/book_history.html', context={'reservs':history_reserv})

        else:
            subject = 'Click here'
            message = "Congratulations, you have successfully subscribed to the newsletter."
            recipient = request.POST.get('email')

            send_mail(subject, message,
                      settings.DEFAULT_FROM_EMAIL, [recipient])
            
            return render(request=request, template_name='main/about.html')

class CarView(ListView):
    model = Car

class CarReservView(LoginRequiredMixin,View):
    model = CarReserv
    template_name = 'main/booking.html'
    form_class = CarReservCreationForm

    def get(self, request):
        carreserv_creation_form = CarReservCreationForm()
        return render(
            request=request,
             template_name='main/booking.html', 
             context={'sign_in_form': carreserv_creation_form,'title': 'Booking'})


    def post(self,request):
        if "booking" in request.POST:
            carreserv_creation_form = CarReservCreationForm(request.POST)

            print(carreserv_creation_form.errors)
            user = User.objects.get(pk=request.user.pk)
            random_idx = randint(0, Car.objects.count() - 1)

            current_date = datetime.now()
            future_date = current_date + timedelta(minutes=30)

            car = Car.objects.filter(booked__lte=current_date+timedelta(minutes=1))[random_idx]

            if carreserv_creation_form.is_valid():
                carreservs = carreserv_creation_form.save(commit=False)
                carreservs.client = user
                carreservs.price =  50 * carreservs.passangers + carreservs.passangers * randint(20, 70)
                carreservs.car = car
                car.booked = future_date

                print(carreserv_creation_form)
                carreservs.save()
                return redirect(
                    'main:history')

            return render(
                request=request,
                template_name='main/booking.html',
                context={'form' :carreserv_creation_form}
            )

        else:
            subject = 'Click here'
            message = "Congratulations, you have successfully subscribed to the newsletter."
            recipient = request.POST.get('email')

            send_mail(subject, message,
                      settings.DEFAULT_FROM_EMAIL, [recipient])
            
            return render(request=request, template_name='main/about.html')

class DriverView(ListView):
    model = Driver

class RateView(ListView):
    model = Rate

class RegistationView(View):
    def get(self, request):
        user_creation_form = CustomUserCreationForm()
        return render(
            request=request,
            template_name='main/registration.html',
            context={'user_form': user_creation_form,'title': 'Sign Up'}
        )

    def post(self, request):
        user_form = CustomUserCreationForm(request.POST) 
        form_errors = [f"{e}: {user_form.errors.get(e, None)[0]}" for e in user_form.errors]

        if user_form.is_valid():
            user_form.save()
            messages.success(request, 'Your account has been created successfuly!')
            return redirect('main:login')

        return render(
            request=request,
            template_name='main/registration.html',
            context={"user_form": user_form,'form_errors':form_errors})

class BookHistoryView(ListView):
    model = CarReserv
    template_name = 'main/book_history.html'
    context_object_name = 'reservs'
    
###############################################
###############################################
