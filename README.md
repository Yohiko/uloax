# Uloax

Author: Yohiko 
Packges: (
    Python,
    SQLite3,
    Django,
    Html,
    CSS,
)


Architecture:
    1. How to run: 
        1. ( activate virtualviev)
        2. (change dir path to 'uloax')
        3. (type 'python manage.py runserver')
    2. FIle with softhware configurations - setting.py

how to install dependencies:
    pip install -r requirements.txt
                or
    pip3 install -r requirements.txt


URLs:
    APP main:
    http://<servername>/uloax -> Main page 
    http://<servername>/uloax/about -> About page keeps information about our company
    http://<servername>/uloax/booking -> Booking page allows you to book a car 
    http://<servername>/uloax/registrations -> Registration page allows you to register new account
    http://<servername>/uloax/login -> Login page allows you to log in your account
    http://<servername>/uloax/logout -> Logout page allows you to log out from your account
    http://<servername>/uloax/history -> History page keeps informations about your booking



